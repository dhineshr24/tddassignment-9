package com.PowerOfFour;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class ExampleTests {

    @Mock
    read file1;
    ValidatePowerOfFour n1;
    ValidatePowerOfFour n2;

    @Spy
    read file2;

    @Before
    public void setup() throws Exception
    {
        System.out.println("start");
        file1 = mock(read.class);
        n1 = new ValidatePowerOfFour();
        n1.set(file1);
    }



    @Test
    public void changingfunction() throws Exception
    {
       when(file1.reader(anyString())).thenReturn(16);
       boolean ans=n1.isPowerOfFour();
       verify(file1).reader(anyString());
       System.out.println(verify(file1).reader(anyString()));
    }


    @Test
    public void isValidPowerOfFour() throws Exception
    {
        when(file1.reader(anyString())).thenReturn(16);
        boolean ans=n1.isPowerOfFour();
        assertTrue(ans);

    }
    @Test
    public void isNotValidPowerOfFour() throws Exception
    {
        n2=new ValidatePowerOfFour();
        file2=spy(new read());
        n2.set(file2);
        doReturn(2).when(file2).reader(anyString());
        boolean ans=ValidatePowerOfFour.isPowerOfFour();
        assertFalse(ans);

    }


}
